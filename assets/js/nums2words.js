/**
 * Created by adaroobi on 03/09/2016.
 */
/*
Formula:
(أ × أ -1) و (ب × ب -1) و (ن × ن-1)

<<---------
100 000 000
234 635 189
  ^
(passed packets = digits/3) Generate the index of current pose suffix[passedDigits/3]
(6/3=2)

مائتين اربع وثلاثون مليون
و
 ستمائة خمس وثلاثون الف
 و
مائة وتسعة وثمانون

 */
// Convert numbers to words

var digitSuffixes  = ['', 'ألف', 'مليون', 'مليار', 'تريليون'];
var multiSuffixes = ['', 'ألفان', 'مليونان', 'ملياران', 'تريليونان'];
var pluralSuffixes = ['', 'آلاف', 'ملايين', 'مليارات', 'تريليونات'];
var digitValues  =  ['صفر', 'واحد', 'اثنان', 'ثلاثة', 'أربعة', 'خمسة', 'ستة', 'سبعة', 'ثمانية', 'تسعة'];
var specialDigitValues  =  ['صفر', 'واحد', 'اثنين', 'ثلاث', 'أربع', 'خمس', 'ست', 'سبع', 'ثمان', 'تسع'];
var digitExceptions = ['عشرة', 'أحد عشر', 'اثنا عشر', 'ثلاثة عشر', 'أربعة عشر', 'خمسة عشر', 'ستة عشر', 'سبعة عشر', 'ثمانية عشر', 'تسعة عشر'];
var digitTens =  ['عشرون', 'ثلاثون', 'أربعون', 'خمسون', 'ستون', 'السبعون', 'ثمانون', 'تسعون'];
var notNumber = 'غير صحيح';
var tooBig = 'الرقم كبير';
var point = ' و ';
var hundred = 'مائة ';
var twoHundred = 'مائتان ';
var thousands = 'آلاف';
var twoThousand = 'ألفان';

function convertNumberToString(number) {

    // Convert Number to a string to allow it's (array split) usage.
    number = number.toString();

    // replace any Comma to a dot to allow detect and validate floats
    number = number.replace(/[\, ]/g, '.');

    // validate its a valid number (and possible float maybe?)
    if (number != parseFloat(number)) return notNumber;

    // store the key index of (.) period occurrence
    var index = number.indexOf('.');

    // no periods detected set the index as the number's length
    if (index == -1) index = number.length;

    // if there are more than 15 digits its out of default range
    // TODO Detect from digitSuffixes array..
    if (index > 15) return tooBig;

    // convert the number to an array (Explode)
    var digitsArray = number.split('');

    // Output string.
    var output = '';

    // Single Number Indicator
    var sk = 0;

    // iterate through the numbers array to make conversion.
    for (var i = 0; i < index; i++) {

        // now get the digits categorization (x MOD 3 Since it flips every 3 digits!)

        // has two digits (ten)s OR (Exceptional)s
        if ((index - i) % 3 == 2) {

            // check the next digit for Arabic reverse numbers reads.

            // case for xx Digits
            if (digitsArray[i+1] != 0 && digitsArray[i+2] == undefined) {

                // Starts with 1x NOT including number 10
                if (digitsArray[i] === '1') {


                    if (sk == 1) {
                        output += ' و ';
                    }

                    // print complete (perfect)
                    output += digitExceptions[Number(digitsArray[i + 1])] + ' ';
                    i++;
                    sk = 1;

                    // check its not x0th
                } else if (digitsArray[i] != 0) {

                    if (sk == 1) {
                        output += ' و ';
                    }

                    // get the next number first
                    output += digitValues[digitsArray[i+1]] + ' و ' + digitTens[digitsArray[i] - 2] + ' ';
                    i++;
                    sk = 1;
                }
            // Figure number 10
            } else if (digitsArray[i] == '1') {

                if (sk == 1) {
                    output += ' و ';
                }

                output += digitExceptions[Number(digitsArray[i + 1])] + ' ';
                i++;
                sk = 1;

            // two digits (xx != 1x != 10) not 10 and not starting with 1 ie. 20 etc..
            } else if (digitsArray[i] != 0) {
                if (sk == 1) {
                    output += ' و ';
                }
                output += digitTens[digitsArray[i] - 2] + ' ';
                sk = 1;
            }
        // address the solo and hundred cases
        } else if (digitsArray[i] != 0) {

            // solve the one hundred and two hundred cases first
            if ((index - i) % 3 == 0) {

                switch (digitsArray[i]) {
                    case '1':
                        output += hundred + '';
                        break;
                    case '2':
                        output += twoHundred + '';
                        break;
                    default:
                        // the remaining hundreds
                        output += specialDigitValues [digitsArray[i]] + ' ' + hundred + ' ';
                        break;
                }
                sk = 1;
            } else {

                // Solve the (و) cases
                if (sk == 1) {
                    output += ' و ';
                }


                if (digitsArray[i+1] == 0) {
                    console.log(digitsArray[i]);
                    switch (digitsArray[i]) {
                        case '1':
                        case '2':
                            break;

                        default:
                            output += digitValues [digitsArray[i]] + ' ';
                            break;
                    }
                } else {
                    // Solo Numbers
                    output += digitValues [digitsArray[i]] + ' ';
                }
                sk = 1;
            }

        }

        // the suffixes
        if ((index - i) % 3 == 1) {
            if (sk && digitSuffixes [(index - i - 1) / 3] != '') {
                // console.log((index - i - 1) / 3);
                output += digitSuffixes [(index - i - 1) / 3] + ' ';
            }
            sk = 0;
        }
    }
    if (index != number.length) {
        var y = number.length;
        output += point;
        for (var i = index + 1; i < y; i++) output += digitValues [digitsArray[i]] + ' ';
    }
    return output.replace(/\s+/g, ' ');
}
